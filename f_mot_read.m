function [my_mot_imp,nFrame_mot,nCoord_mot] = f_mot_read(file_mot,time_pick)
if nargin < 2
    time_pick=[-1 1]*1e9;
end

% file_mot='temp3.mot';
% file_mot='C:\OpenSim_4\model_1\Models\Gait2392_Simbody_Tim_Sayer\Subj_spec_model_data\Dev36\Barefoot\Dev36_Running_03_cut.mot';
[my_mot_imp,~,~] = importdata(file_mot);

if ~isfield(my_mot_imp,'colheaders')
    temp_colheaders=my_mot_imp.textdata{end};
    my_mot_imp.colheaders=strsplit(temp_colheaders,'\t');
end


my_mot_imp.colheaders=my_mot_imp.colheaders(2:end);

time_all=my_mot_imp.data(:,1);

indx_time=time_all>=time_pick(1)&time_all<=time_pick(2);
my_mot_imp.data = my_mot_imp.data (indx_time,:);
my_mot_imp.time=my_mot_imp.data(:,1);
my_mot_imp.data(:,1) = [];
my_mot_imp.data_deg= my_mot_imp.data;
my_mot_imp.data= my_mot_imp.data/180*pi;

[nFrame_mot, nCoord_mot] = size(my_mot_imp.data);
end

