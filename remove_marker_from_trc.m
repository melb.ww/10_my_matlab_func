function [ trc ] = remove_marker_from_trc( trc,varargin )

nVarargs = length(varargin);

for k = 1:nVarargs
%     trc.MarkerList=[trc.MarkerList,varargin{k}.name];
%     trc.Data=[trc.Data,varargin{k}.data];
    
    Ind=find(ismember(trc.MarkerList, varargin{k}));
    if(Ind)
        trc.MarkerList(Ind)=[];
        trc.Data(:,3*Ind:3*Ind+2)=[];
    else
        disp(['Error!!!:' varargin{k},' Marker not found']);
        break
    end
end

end
