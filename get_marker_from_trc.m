function [ marker ] = get_marker_from_trc( trc,imarker )



index=find(ismember(trc.MarkerList, imarker));
marker.index=index;
if isempty(index)
    disp([imarker,' Marker not found']);
    marker=[];
    return
end
if length(index)>1
    disp([imarker, '  More than one']);
    return
end

marker.name=imarker;
marker.data=trc.Data(:,3*index:3*index+2);



end
