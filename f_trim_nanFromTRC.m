function f_trim_nanFromTRC(file_input,file_output)
thresH=0.85;
trc_input=read_trcFile(file_input);
trc_dataonly=trc_input.Data(:,3:end);
trc_nanFlag=~isnan(trc_dataonly);
nanScore=mean(trc_nanFlag,2);


temp_indx=find(nanScore>thresH);
ind_start=temp_indx(1);
ind_end=temp_indx(end);
trc_data_out=trc_dataonly(ind_start:ind_end,:);
col1=[1:size(trc_data_out,1)]';
col2=trc_input.Data(ind_start:ind_end,2);

trc_output=trc_input;
trc_output.Data=[col1,col2,trc_data_out];
trc_output.OrigNumFrames=size(trc_output.Data,1);
trc_output.NumFrames=size(trc_output.Data,1);


make_trcFile(file_output,trc_output);
end