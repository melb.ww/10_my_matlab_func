function [vq] = f_resamp_inter1(data_input,length_targ)

if size(data_input,1)==1
    data_input=data_input';
end

x=linspace(1,100,size(data_input,1))';
xq=linspace(1,100,length_targ)';
vq = interp1(x,data_input,xq);

end

