function [path_outputArg] = f_get_path_same_level(inputArg1)
path_now=pwd;
indx_lash=find(path_now=='\');
path_upper=path_now(1:indx_lash(end));
path_outputArg = [path_upper,inputArg1];
end