function [dir_folders] = f_dirGetFolders(foldername)
dir_folders=dir(foldername);
dir_folders= dir_folders([dir_folders(:).isdir]~=0);
dir_folders = dir_folders(~contains({dir_folders(:).name},{'.','..'}));
end

