function f_plot_mot(my_mot_GT,plot_colheaders)
% plot_colheaders={'hip_flexion_r','knee_angle_r','ankle_angle_r'};
p=f_numSubplots(length(plot_colheaders));
for n=1:length(plot_colheaders)
    subplot(p(1),p(2),n)
%     ind_temp=contains(my_mot_GT.colheaders,plot_colheaders{n});
    ind_temp=strcmp(my_mot_GT.colheaders,plot_colheaders{n});
    if isfield(my_mot_GT,'data_deg')
        plot(my_mot_GT.time,my_mot_GT.data_deg(:,ind_temp));
    else
        plot(my_mot_GT.time,my_mot_GT.data(:,ind_temp));
    end
%     plot(my_mot_GT.data_deg(:,ind_temp));
    hold on;
    title_no_interpreter(plot_colheaders{n});
end
end

