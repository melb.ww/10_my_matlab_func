function [ y ] = JR3_Aniso_mtlb( JRF )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
% JRF(1)=JRF(1);
% JRF(2)=JRF(2);
% JRF(3)=JRF(3);
%     JRF(1)=reactionForces[JointIndex](1)[0];
%     JRF(2)=reactionForces[JointIndex](1)[1];
%     JRF(3)=reactionForces[JointIndex](1)[2];
% //     norm=sqrt((JRF(1) * JRF(1)) + (JRF(2) * JRF(2)) + (JRF(3) * JRF(3)));
norm_magnitude=(JRF(1)*0.6552+JRF(2)*0.0951+JRF(3)*0.7495);
shear_h=(JRF(1)*0.6441-JRF(2)*0.5888-JRF(3)*0.4883);
shear_v=(JRF(1)*0.3949+JRF(2)*0.8026-JRF(3)*0.4471);


%     0.34=0.34;
% 0.61=0.61;

y=shear_h*shear_h/(norm_magnitude*0.34)/(norm_magnitude*0.34)+shear_v*shear_v/(norm_magnitude*0.61)/(norm_magnitude*0.61);




end

