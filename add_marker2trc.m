function [ trc ] = add_marker2trc( trc,varargin )

nVarargs = length(varargin);

for k = 1:nVarargs
    trc.MarkerList=[trc.MarkerList,varargin{k}.name];
    trc.Data=[trc.Data,varargin{k}.data];
end

end

