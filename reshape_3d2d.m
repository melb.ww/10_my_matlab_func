function [ output_args ] = reshape_3d2d( input_args )
[row,~,~]=size(input_args);
output_args=[];
for rol_ind=1:row
    output_args=[output_args;squeeze(input_args(rol_ind,:,:))'];
end
end

