function [output_grf_norm] = f_norm_mat(output_grf,min_set,max_set)
if nargin==3
    set_min_repmat=repmat(min_set,size(output_grf,1),1);
    set_max_repmat=repmat(max_set,size(output_grf,1),1);
else
    set_max_repmat=repmat(max(output_grf),size(output_grf,1),1);
    set_min_repmat=repmat(min(output_grf),size(output_grf,1),1);
end
output_grf_norm=(output_grf-set_min_repmat)./(set_max_repmat-set_min_repmat);


end