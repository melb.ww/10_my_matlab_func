function  f_write_cell2xml(filename,cell_new)
fid = fopen(filename, 'w');
for i = 1:numel(cell_new)
    if cell_new{i+1} == -1
        fprintf(fid,'%s', cell_new{i});
        break
    else
        fprintf(fid,'%s\n', cell_new{i});
    end
end
fclose(fid);
disp(['file written to --- ', filename])
end

