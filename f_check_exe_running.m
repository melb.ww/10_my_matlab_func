function  flag_running=f_check_exe_running(exe_name)
[~,result] = system(['tasklist /FI "imagename eq ',exe_name,'" /fo table /nh']);
flag_running = ~prod(result(1:8)=='INFO: No');
end