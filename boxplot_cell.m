function [matFromcell,groupFromcell] = boxplot_cell( mycell)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
N=length(mycell);
matFromcell=[];
groupFromcell=[];
for n=1:N
    temp=cell2mat(mycell(n));
    temp2=ones(size(temp,2),1)*n;
    matFromcell=[matFromcell;temp'];
    groupFromcell=[groupFromcell;temp2];
end
% boxplot(matFromcell,groupFromcell)
end

% end

