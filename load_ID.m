% IDsetupFile='C:\Users\wenw1\Desktop\1_PHDing!\1_matlab_pieces\1_MTP_Opti\2_later_api_opti\setup_id_api_no_model_no_external.xml';

IDsetupFile=[fileparts(pwd),'\1_MTP_Opti\2_later_api_opti\setup_id_api_no_model_no_external.xml'];
IDtool=InverseDynamicsTool(IDsetupFile);
IDtool.setResultsDir([fileparts(pwd),'\7_forTemp\ID_temp']);
IDtool.setModel(my_model)
IDtool=my_Set_ID_mot(IDtool,motionfilename);
IDtool.run()

% obtain ID data-----------
char(IDtool.getResultsDir());
IDtool.getOutputGenForceFileName();
full_ID_file=[char(IDtool.getResultsDir()),'\',char(IDtool.getOutputGenForceFileName())];
my_ID_imp=importdata(full_ID_file);
my_ID_imp.colheaders(:,1) = [];
my_ID_imp.data(:,1) = [];
% get interested ID datae

nCoor_SO=length(Free_joints);
T_interest=[]
for n=1:nCoor_SO
    temp_index=my_model.getCoordinateSet().getIndex(char(Free_joints(n)))+1;
    T_interest=[T_interest,my_ID_imp.data(:,temp_index) ];
end

