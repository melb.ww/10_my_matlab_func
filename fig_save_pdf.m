function fig_save_pdf(handle, filename, r, width, height)
% r = 700; % pixels per inch
set(handle, 'Color', 'white'); % white bckgr
set(handle, 'PaperUnits', 'inches', 'PaperPosition', [0 0 width height]);
% print(handle,'-djpeg',['-r',num2str(r)], filename);
% print(handle,'-depsc', '-painters',[filename,'.eps']);
print(handle,'-dpdf', '-painters',[filename,'.pdf']);
% print(handle,'-dpdf', '-painters',filename);
% print(handle,'-dpdf',['-r',num2str(r)], filename);
% winopen([filename,'.eps'])
winopen([filename,'.pdf'])
[filename,'.pdf']
% close (handle)
end