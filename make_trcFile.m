%------------ Written by Wen Unimelb 2015-----------
function [  ] = make_trcFile( trc_file_name,trc)

dataheader1 = 'Frame#\tTime\t';
dataheader2 = '\t\t';
format_text = '%i\t%2.4f\t';

for i = 1:(size(trc.Data,2)-2)/3
    dataheader1 = [dataheader1 trc.MarkerList{i} '\t\t\t'];
    dataheader2 = [dataheader2 'X' num2str(i) '\t' 'Y' num2str(i) '\t' 'Z' num2str(i) '\t'];
    format_text = [format_text '%f\t%f\t%f\t'];
end
dataheader1 = [dataheader1 '\n'];
dataheader2 = [dataheader2 '\n'];
format_text = [format_text '\n'];

% write the file
fid_1 = fopen(trc_file_name,'w');
fprintf(fid_1,'PathFileType\t4\t(X/Y/Z)\t %s\n',trc_file_name);
fprintf(fid_1,'DataRate\tCameraRate\tNumFrames\tNumMarkers\tUnits\tOrigDataRate\tOrigDataStartFrame\tOrigNumFrames\n');
fprintf(fid_1,'%d\t%d\t%d\t%d\t%s\t%d\t%d\t%d\n', trc.DataRate, trc.CameraRate, size(trc.Data,1),(size(trc.Data,2)-2)/3, trc.Units, trc.OrigDataRate, trc.OrigDataStartFrame,trc.OrigNumFrames);
fprintf(fid_1, dataheader1);
fprintf(fid_1, dataheader2);
fprintf(fid_1, format_text,trc.Data');
fclose(fid_1);

disp(['TRC saved to: ',trc_file_name])

end