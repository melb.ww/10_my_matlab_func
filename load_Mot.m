% motionfilename=IDtool.getCoordinatesFileName();
% motionFile = importdata('C:\Users\wenw1\Desktop\1_PHDing!\1_matlab_pieces\api_muscle_test\elbow.mot');
[my_mot_imp,delimiterOut,headerlinesOut] = importdata(motionfilename);
time=my_mot_imp.data(:,1);
my_mot_imp.colheaders(:,1) = [];
my_mot_imp.data(:,1) = [];

% my_mot_imp.data=interp1(time_temp,my_mot_imp.data,time);


my_mot_imp.data=my_mot_imp.data/180*pi;
% read the motion details
[nFrame_mot, nCoord_mot] = size(my_mot_imp.data);