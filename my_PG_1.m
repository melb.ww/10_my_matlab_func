clear all
filename='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\Timothy_sayer_data\Tim cleaned\Dev01\Barefoot\Dev01 Running 03.c3d';
channelname='aaRecFem';


% ------------Read-------------
itf = c3dserver();
openc3d(itf, 0, filename);
% ---------Preparation------------

ratio=itf.GetAnalogVideoRatio;
n_frame=itf.GetVideoFrameRate;
EMG=f_getanalogchannel(itf, channelname);
 time_EMG=linspace(0,length(EMG)/ratio/n_frame,length(EMG));

%%
% ---------Processing----------


EMG_p=double(EMG);
figure;plot (EMG_p)

% removeDC
EMG_p = detrend(EMG_p);

%  'HPF' 
order = 2;
freq = 25;
[b_l,a_l] = butter(order, freq/(ratio*n_frame/2), 'high');
EMG_p = filtfilt(b_l, a_l, EMG_p);

% rectify
EMG_p=abs(EMG_p);

%  'LPF' 
order = 2;
freq = 2;
[b_l,a_l] = butter(order, freq/(ratio*n_frame/2), 'low');
EMG_p = filtfilt(b_l, a_l, EMG_p);

figure;plot (EMG_p)