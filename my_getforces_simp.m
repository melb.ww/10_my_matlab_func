function [ F_act_alongtendon,F_pas_alongtendon ] = my_getforces_simp( a,Lmt,lm_dot )
%% globals
global Lom Lst Fom Vmax

nor_length=(Lmt-Lst)./Lom;
l_temp=exp( -0.5.*((nor_length-1)/0.19).^2);
Fv = 0.1433./ ( 0.1074+exp(-1.409.*sinh(3.2.*lm_dot+1.6 )));
F_act_alongtendon=Fom.*l_temp.*a'.*Fv;
F_pas_alongtendon=Fom.*exp(10.*(nor_length-1))./exp(5);

%     Fm=Fa.*acti(n_count,:)+Fp;
%
%
% for index_mus=1:nMusc
%     my_musc_set.get(index_mus-1).setActivation(myState, a(index_mus));
% end
% my_model.equilibrateMuscles(myState);
% for index_mus=1:nMusc
%     F_act_alongtendon(index_mus)=my_musc_set.get(index_mus-1).getActiveFiberForceAlongTendon(myState);
%     F_pas_alongtendon(index_mus)=my_musc_set.get(index_mus-1).getPassiveFiberForceAlongTendon(myState);
% end










end

