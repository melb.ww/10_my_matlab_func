my_model=Model(ModelFile);
myState =  my_model.initSystem();

% ----muscle details
my_musc_set=my_model.getMuscles();
nMusc = my_musc_set.getSize();
my_musc_names=cell(nMusc,1);
for n=1:nMusc
    my_musc_names(n)=my_musc_set.get(n-1).getName();
    Lom(n)=my_musc_set.get(n-1).getOptimalFiberLength();
    Fom(n)=my_musc_set.get(n-1).getMaxIsometricForce();
    Lst(n)=my_musc_set.get(n-1).getTendonSlackLength();
    Vmax(n)=Lom(n)*my_musc_set.get(n-1).getMaxContractionVelocity();
end
if exist('disable_muscle_ind','var')
    Fom(disable_muscle_ind)=0;
end
% ----coord details
my_coor_set=my_model.getCoordinateSet();
nCoord_all = my_coor_set.getSize();
my_coord_names=cell(nCoord_all,1);
for n=1:nCoord_all
    my_coord_names(n)=my_coor_set.get(n-1).getName();
end

