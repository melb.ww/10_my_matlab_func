function [ output_args ] = reshape_3d2d_list( input_args )
[~,~,Dim_3]=size(input_args);
output_args=[];
for Dim_ind=1:Dim_3
    output_args=[output_args;input_args(:,:,Dim_ind)];
end
end

