function [un_normed] = f_norm_mat_inv(tobe_inv,min_set_or_orig,max_set)
if nargin==3
    set_min_repmat=repmat(min_set_or_orig,size(tobe_inv,1),1);
    set_max_repmat=repmat(max_set,size(tobe_inv,1),1);
end
if nargin==2
    set_max_repmat=repmat(max(min_set_or_orig),size(tobe_inv,1),1);
    set_min_repmat=repmat(min(min_set_or_orig),size(tobe_inv,1),1);
end
    
un_normed=tobe_inv.*(set_max_repmat-set_min_repmat)+set_min_repmat;

end