function [  ] = f_send_email( subject,theMessage,attachments )
narginchk(1,3);
if (nargin < 2)
    theMessage = '';
end
if (nargin < 3) 
    attachments = [];
elseif ischar(attachments)
    attachments = {attachments};
end

setpref('Internet','E_mail','wm.melb@gmail.com');
setpref('Internet','SMTP_Server','smtp.gmail.com');
setpref('Internet','SMTP_Username','wm.melb@gmail.com');
setpref('Internet','SMTP_Password','1qaz8ikm');
props=java.lang.System.getProperties;
props.setProperty('mail.smtp.auth','true');
props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
props.setProperty('mail.smtp.socketFactory.port','465');


historypath = com.mathworks.mlservices.MLCommandHistoryServices.getSessionHistory;
M_name_temp = historypath(end);

sendmail('melb.ww@gmail.com',...
    ['"',char(M_name_temp),'.m"',subject],...
    theMessage,...
    attachments);

disp('============ Email sent to melb.ww@gmail.com ==============')
disp(theMessage)
disp(attachments)
end

